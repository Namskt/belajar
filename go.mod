module server

go 1.21.3

require (
	github.com/go-chi/chi/v5 v5.0.10 // indirect
	github.com/google/uuid v1.5.0 // indirect
	github.com/lib/pq v1.10.9 // indirect
)
