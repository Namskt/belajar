package routers

import (
	"server/app/handlers"

	"github.com/go-chi/chi/v5"
)

func UsersRouters(r chi.Router) {
	r.Route("/users", func(r chi.Router) {
		r.Post("/role", handlers.CreatedRole)
		r.Post("/permission", handlers.CreatedPermission)
		r.Post("/user", handlers.CreatedUser)
		r.Get("/getusers", handlers.GetAllUsers)
	})
}
