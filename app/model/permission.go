package model

import "time"

type Permission struct {
	ID         string
	Name       string
	Created_At time.Time
}
