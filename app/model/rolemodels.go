package model

import "time"

type Role struct {
	ID         string
	Name       string
	Created_At time.Time
}
