package model

import (
	"time"
)

type User struct {
	ID            string
	Username      string
	Password      string
	Role_ID       string
	Role          Role
	Permission_ID string
	Permission    Permission
	Created_At    time.Time
}
