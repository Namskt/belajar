package handlers

import (
	"encoding/json"
	"net/http"
	"server/app/database"
	"server/app/model"
	"time"

	"github.com/google/uuid"
)

func CreatedUser(w http.ResponseWriter, r *http.Request) {
	var user model.User
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	NewUUID := uuid.New()
	user.ID = NewUUID.String()

	query := "INSERT INTO users (id, username, password, role_id, permission_id, created_at) VALUES ($1, $2, $3, $4, $5, $6)"
	_, err = database.DB.Exec(query, user.ID, user.Username, user.Password, user.Role_ID, user.Permission_ID, time.Now())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	roleQuery := "SELECT id, name, created_at FROM role WHERE id = $1"
	err = database.DB.QueryRow(roleQuery, user.Role_ID).Scan(&user.Role.ID, &user.Role.Name, &user.Role.Created_At)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	permissionQuery := "SELECT id, name, created_at FROM permission WHERE id = $1"
	err = database.DB.QueryRow(permissionQuery, user.Permission_ID).Scan(&user.Permission.ID, &user.Permission.Name, &user.Permission.Created_At)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(user)
}

func GetAllUsers(w http.ResponseWriter, r *http.Request) {
	rows, err := database.DB.Query("SELECT id, username, password, role_id, permission_id, created_at FROM users")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	var users []model.User
	for rows.Next() {
		var user model.User
		err := rows.Scan(&user.ID, &user.Username, &user.Password, &user.Role_ID, &user.Permission_ID, &user.Created_At)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		roleQuery := "SELECT id, name, created_at FROM role WHERE id = $1"
		err = database.DB.QueryRow(roleQuery, user.Role_ID).Scan(&user.Role.ID, &user.Role.Name, &user.Role.Created_At)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		permissionQuery := "SELECT id, name, created_at FROM permission WHERE id = $1"
		err = database.DB.QueryRow(permissionQuery, user.Permission_ID).Scan(&user.Permission.ID, &user.Permission.Name, &user.Permission.Created_At)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		users = append(users, user)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(users)
}
