package handlers

import (
	"encoding/json"
	"net/http"
	"server/app/database"
	"server/app/model"
	"time"

	"github.com/google/uuid"
)

func CreatedPermission(w http.ResponseWriter, r *http.Request) {
	var permission model.Permission
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&permission)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	NewUUID := uuid.New()
	permission.ID = NewUUID.String()

	query := "INSERT INTO permission (id, name, created_at) VALUES ($1, $2, $3)"
	_, err = database.DB.Exec(query, permission.ID, permission.Name, time.Now())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(permission)
}
