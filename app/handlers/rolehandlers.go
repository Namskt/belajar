package handlers

import (
	"encoding/json"
	"net/http"
	"server/app/database"
	"server/app/model"
	"time"

	"github.com/google/uuid"
)

func CreatedRole(w http.ResponseWriter, r *http.Request) {
	var role model.Role
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&role)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	NewUUID := uuid.New()
	role.ID = NewUUID.String()

	query := "INSERT INTO role (id, name, created_at) VALUES ($1, $2, $3)"
	_, err = database.DB.Exec(query, role.ID, role.Name, time.Now())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(role)
}
