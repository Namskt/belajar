package main

import (
	"log"
	"net/http"
	"server/app/routers"

	"github.com/go-chi/chi/v5"
)

func main() {
	r := chi.NewRouter()
	routers.UsersRouters(r)
	err := http.ListenAndServe(":5000", r)
	if err != nil {
		log.Fatal(err)
	}
}
